var app = angular.module('bookApp', ["firebase"])
	app.controller('BookListController', function($scope, $firebaseArray) {
		var ref = new Firebase("https://unspoiled.firebaseio.com/");

		// download the data into a local object
		$scope.seriesList = $firebaseArray(ref);

		// synchronize the object with a three-way data binding
		$scope.addSeries = function() {
			$scope.seriesList.$add({
				title: $scope.newSeriesTitle,
				author: $scope.newSeriesAuthor,
				genre: $scope.newSeriesGenre,
				cover: $scope.newSeriesCover
			});
		}


	});